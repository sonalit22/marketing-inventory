const headers = { 'Content-Type': 'application/json' };

function _checkStatus (response) {
  if (response.ok) {
    return response;
  } else {
    const error = new Error(response.statusText);
    error.respnse = response;

    throw error;
  }
}

function _parseJSON (response) {
  return response.json()
    .catch(err => {
      return response;
    });
}

export const fetchZones = (city) => {

  return fetch(`/api/getAreas/${city}`,
    {
      method: 'GET',
      headers: new Headers(headers),
    })
        .then(response => {
          return response;
        }).then(_checkStatus)
          .then(_parseJSON);
};

export const fetchTargetRevenue = (city) => {

  return fetch(`/api/getTargetRevenue/${city}`,
    {
      method: 'GET',
      headers: new Headers(headers),
    })
        .then(response => {
          return response;
        }).then(_checkStatus)
          .then(_parseJSON);
};

export const fetchListing = (filters = {}) => {
  const {
    city, zone, startDate, endDate, slot, priority, restaurantId, parentId, availability
  } = filters;
  
  const parent_ids = parentId !== undefined ? [parentId] : [];
  const restaurant_ids = restaurantId !== undefined ? [restaurantId] : [];

  filters = {
    city,
    zone: [zone],
    from_date: startDate,
    to_date: endDate,
    meal_time: [slot],
    parent_ids,
    position: priority,
    restaurant_ids,
    status: availability
  };

  return fetch('/api/ticketListing', { method: 'POST', headers: new Headers(headers), body: JSON.stringify(filters) })
        .then(response => {
          return response;
        }).then(_checkStatus)
          .then(_parseJSON);
};
