import React from 'react';
import PropTypes from 'prop-types';
import styles from './CommonHeader.scss';
import classNames from 'classnames';

export default class Listing extends React.Component {
  render () {
    const navBarClass = classNames({
      [styles['navbar-inverse']]: true,
      'navbar': true,
    });
        const navBarBrand = classNames({
          [styles['navbar-brand']]: true,
        });

    return (
      <nav className={navBarClass}>
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand" href="#">Swiggy</a>
          </div>
          <ul className="nav navbar-nav navbar-right">
            <li><a href="#">Welcome XYZ</a></li>
            <li><a href="#">Rate Card</a></li>
            <li><a href="#">Logout</a></li>
          </ul>
        </div>
      </nav>
    );
  }
}
