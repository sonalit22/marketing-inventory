import { combineReducers } from 'redux'
import locationReducer from './reducers/location'
import filterReducer from './reducers/filter';
import listingReducer from './reducers/listing';

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    listing: listingReducer,
    filter: filterReducer,
    location: locationReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
}

export default makeRootReducer;
