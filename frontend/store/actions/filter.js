import * as ACTIONS from './types/filter';
import { FETCH_SLOT_REVENUE } from './types/listing';
import {
    fetchZones as _fetchZones,
    fetchTargetRevenue
} from 'helpers/Api';

import { fetchListing } from './listing';

export function changeStartDate (date) {
  return {
    type: ACTIONS.CHANGE_START_DATE,
    payload: date
  };
}

export function changeEndDate (date) {
  return {
    type: ACTIONS.CHANGE_END_DATE,
    payload: date
  };
}
export function changeCity (value) {
  return (dispatch, getState) => {
    dispatch({ type: ACTIONS.CHANGE_CITY, payload: value });
    Promise.all([_fetchZones(value), fetchTargetRevenue(value)])
      .then(responses => {
        const [zone, slotRevenue] = responses;
        dispatch({ type: ACTIONS.SET_ZONES, payload: zone.data });
        dispatch({ type: FETCH_SLOT_REVENUE, payload: slotRevenue.data });

        const [defaultZone] = zone.data || [];
        if (defaultZone && defaultZone.areaName) {
          dispatch({ type: ACTIONS.CHANGE_ZONE, payload: defaultZone.areaName });
          fetchListing()(dispatch, getState);
        }
      })
      .catch(err => {});
  };
}
export function changeZone (value) {
  return {
    type: ACTIONS.CHANGE_ZONE,
    payload: value
  };
}

export function changePriority (value) {
  return {
    type: ACTIONS.CHANGE_PRIORITY,
    payload: value
  };
}

export function changeAvailability (value) {
  return {
    type: ACTIONS.CHANGE_AVAILABILITY,
    payload: value
  };
}
export function changeSlot (value) {
  return {
    type: ACTIONS.CHANGE_SLOT,
    payload: value
  };
}

export function changeParentId (value) {
  return {
    type: ACTIONS.CHANGE_PARENT_ID,
    payload: value
  };
}
export function changeRestaurantId (value) {
  return {
    type: ACTIONS.CHANGE_RESTAURANT_ID,
    payload: value
  };
}
