import moment from 'moment';
import * as ACTIONS from './types/listing';
import {
    fetchListing as _fetchListing
} from 'helpers/Api';

export function fetchListing () {
  return (dispatch, getState) => {
    const filter = getState().filter;
    const city = filter.city.value;
    const zone = filter.zone.value;
    const startDate = moment(filter.startDate.value).format('YYYY-MM-DD');
    const endDate = moment(filter.endDate.value).format('YYYY-MM-DD');
    const slot = filter.slot.value;
    const priority = filter.priority.value;
    const restaurantId = filter.restaurantId.value;
    const parentId = filter.parentId.value;
    const availability = filter.availability.value;

    _fetchListing({
      city,
      zone,
      startDate,
      endDate,
      slot,
      priority,
      restaurantId,
      parentId,
      availability
    })
        .then(response => {
          dispatch({ type: ACTIONS.FETCH_LISTING, payload: response.data });
        })
     .catch(err => {});
  };
}
