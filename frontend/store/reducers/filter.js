import * as ACTIONS from '../actions/types/filter';
import moment from 'moment';

const tomorrow = moment().add(1, 'day').toDate();
const tomorrowNextWeek = moment().add(1, 'day').add(1, 'week').toDate();

const initialState = {
  startDate: {
    key: 'start_date',
    value: tomorrow
  },
  endDate: {
    key: 'end_date',
    value: tomorrowNextWeek
  },
  city: {
    key: 'cityName',
    list: ['Bangalore', 'Chennai', 'Hyderabad', 'Delhi', 'Gurgaon', 'Mumbai', 'Pune', 'Kolkata']
  },
  zone: {
    key: 'zone',
    valueKey: 'areaName',
    codeKey: 'areaName',
    list: []
  },
  priority: {
    key: 'priority',
    list: [1, 2, 3, 4, 5, 6]
  },
  availability: {
    key: 'availability',
    value: 'AVAILABLE',
    list: ['BOOKED', 'AVAILABLE']
  },
  slot: {
    key: 'slot',
    value: 'ALL',
    list: [{"id":"ALL","value":"All Day"},{"id":"BREAKFAST","value":"Breakfast"},{"id":"LUNCH","value":"Lunch"},{"id":"DINNER","value":"Dinner"},{"id":"LATE_NIGHT","value":"Late Night"}],
    valueKey: 'value',
    codeKey: 'id',
  },
  parentId: {
    key: 'parent_id'
  },
  restaurantId: {
    key: 'restaurant_id'
  }
};

const ACTION_HANDLER = {
  [ACTIONS.CHANGE_START_DATE]: (state, action) => {
    const startDate = { ...state.startDate, ...{ value: action.payload } };
    return {
      ...state,
      ...{ startDate }
    };
  },
  [ACTIONS.CHANGE_END_DATE]: (state, action) => {
    const endDate = { ...state.endDate, ...{ value: action.payload } };
    return {
      ...state,
      ...{ endDate }
    };
  },
  [ACTIONS.CHANGE_CITY]: (state, action) => {
    const city = { ...state.city, ...{ value: action.payload } };
    return {
      ...state,
      ...{ city }
    };
  },
  [ACTIONS.CHANGE_PRIORITY]: (state, action) => {
    const priority = { ...state.priority, ...{ value: action.payload } };
    return {
      ...state,
      ...{ priority }
    };
  },
  [ACTIONS.CHANGE_AVAILABILITY]: (state, action) => {
    const availability = { ...state.availability, ...{ value: action.payload } };
    return {
      ...state,
      ...{ availability }
    };
  },
  [ACTIONS.CHANGE_SLOT]: (state, action) => {
    const slot = { ...state.slot, ...{ value: action.payload } };
    return {
      ...state,
      ...{ slot }
    };
  },
  [ACTIONS.CHANGE_PARENT_ID]: (state, action) => {
    const parentId = { ...state.parentId, ...{ value: action.payload } };
    return {
      ...state,
      ...{ parentId }
    };
  },
  [ACTIONS.CHANGE_RESTAURANT_ID]: (state, action) => {
    const restaurantId = { ...state.restaurantId, ...{ value: action.payload } };
    return {
      ...state,
      ...{ restaurantId }
    };
  },
  [ACTIONS.CHANGE_ZONE]: (state, action) => {
    const zone = { ...state.zone, ...{ value: action.payload } };
    const finalState = {
      ...state,
      zone
    };
    return finalState;
  },
  [ACTIONS.SET_ZONES]: (state, action) => {
    const zone = { ...state.zone, ...{ list: action.payload } };
    return {
      ...state,
      ...{ zone }
    };
  }
};

export default function filterReducer (state = initialState, action) {
  const handler = ACTION_HANDLER[action.type];
  return handler ? handler(state, action) : state;
}
