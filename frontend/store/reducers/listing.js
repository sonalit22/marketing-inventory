
import * as ACTIONS from '../actions/types/listing';

const initialState = {
  list: [],
  slotRevenue:[]
};

const ACTION_HANDLERS = {
  [ACTIONS.FETCH_LISTING]: (state, action) => ({ ...state, list: action.payload }),
  [ACTIONS.FETCH_SLOT_REVENUE]: (state, action) => ({ ...state, slotRevenue: action.payload })
};

export default function listingReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};
