import { FILTER_TYPES } from '../constants';

export function getListingTableConfig () {
  return [
    {
      path: 'ticket.date',
      headerText: 'Date',
      width: 100,
      type: 'date',
      alignment: 'left'
    },
    {
      path: 'ticket.mealTime',
      headerText: 'Slot',
      width: 100,
      type: 'text',
      alignment: 'left'
    },
    {
      path: 'ticket.area.cityId',
      headerText: 'City',
      width: 100,
      type: 'city',
      alignment: 'left'
    },
    {
      path: 'ticket.area.zone',
      headerText: 'Zone',
      width: 100,
      type: 'zone',
      alignment: 'left'
    },
    {
      path: 'position',
      headerText: 'Priority',
      width: 100,
      type: 'priority',
      alignment: 'left'
    },
    {
      path: 'status',
      headerText: 'Availability',
      width: 100,
      type: 'availability',
      alignment: 'left'
    },
    {
      path: 'surgeCode',
      headerText: 'Surge',
      width: 100,
      type: 'text',
      alignment: 'center'
    }
  ];
}

export function getFilterConfiguration () {
  return [
    {
      type: FILTER_TYPES.DATE,
      label: 'Start Date',
      filterKey: 'start_date',
      stateKey: 'startDate',
      onChange: 'changeStartDate'
    },
    {
      type: FILTER_TYPES.DATE,
      label: 'End Date',
      filterKey: 'end_date',
      stateKey: 'endDate',
      onChange: 'changeEndDate'
    },
    {
      type: FILTER_TYPES.DROPDOWN,
      label: 'City',
      filterKey: 'city',
      stateKey: 'city',
      onChange: 'changeCity'
    },
    {
      type: FILTER_TYPES.DROPDOWN,
      label: 'Zone',
      filterKey: 'zone',
      stateKey: 'zone',
      onChange: 'changeZone'
    },
    {
      type: FILTER_TYPES.DROPDOWN,
      label: 'Priority',
      filterKey: 'priority',
      stateKey: 'priority',
      onChange: 'changePriority'
    },
    {
      type: FILTER_TYPES.INPUT,
      label: 'Parent Id',
      filterKey: 'parent_id',
      stateKey: 'parentId',
      onChange: 'changeParentId'
    },
    {
      type: FILTER_TYPES.INPUT,
      label: 'Restaurant Id',
      filterKey: 'restaurant_Id',
      stateKey: 'restaurantId',
      onChange: 'changeRestaurantId'
    },
    {
      type: FILTER_TYPES.DROPDOWN,
      label: 'Availability',
      filterKey: 'availability',
      stateKey: 'availability',
      onChange: 'changeAvailability'
    },
    {
      type: FILTER_TYPES.DROPDOWN,
      label: 'Slot',
      filterKey: 'slot',
      stateKey: 'slot',
      onChange: 'changeSlot'
    }
  ];
}
