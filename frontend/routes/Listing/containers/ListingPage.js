import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchListing } from 'store/actions/listing';

import ListingPage from 'routes/Listing/components/ListingPage';

const mapDispatchToProps = dispatch => bindActionCreators({ fetchListing }, dispatch);

export default connect(null, mapDispatchToProps)(ListingPage);
