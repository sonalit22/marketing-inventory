import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchListing } from 'store/actions/listing';

import ListingTable from '../components/ListingTable';

const mapDispatchToProps = dispatch => bindActionCreators({ fetchListing }, dispatch);
const mapStateToProps = (state) => ({
  list: state.listing.list
});

export default connect(mapStateToProps, mapDispatchToProps)(ListingTable);
