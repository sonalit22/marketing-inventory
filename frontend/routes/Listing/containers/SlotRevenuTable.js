import { connect } from 'react-redux';
import SlotRevenueTable from '../components/SlotRevenueTable';

const mapStateToProps = (state) => ({
  list: state.listing.slotRevenue
});

export default connect(mapStateToProps)(SlotRevenueTable);
