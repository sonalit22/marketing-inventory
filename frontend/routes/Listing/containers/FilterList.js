import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { changeCity } from 'store/actions/filter';

import FilterList from '../components/FilterList';

const mapDispatchToProps = dispatch => bindActionCreators({ changeCity }, dispatch);

export default connect(null, mapDispatchToProps)(FilterList);
