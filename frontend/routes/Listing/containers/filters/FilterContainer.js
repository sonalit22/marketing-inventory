import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as filterActions from 'store/actions/filter';
import FilterContainer from 'routes/Listing/components/filters/FilterContainer';

const mapDispatchToProps = dispatch => bindActionCreators({ ...filterActions }, dispatch);
const mapStateToProps = (state) => ({
  filter: state.filter
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterContainer);

