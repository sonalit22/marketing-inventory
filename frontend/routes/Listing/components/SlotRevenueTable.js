import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { CSVLink } from 'react-csv';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

const styles = {
  propContainer: {
    width: 200,
    overflow: 'hidden',
    margin: '20px auto 0',
    backgroundColor:'#000'
  },
  propToggleHeader: {
    margin: '20px auto 10px'
  }
};

const wrapperStyle = {
  marginTop:'70px',
  marginBottom:'30px'
};


class SlotRevenueTable extends PureComponent {
  defaultState = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: false,
    showCheckboxes: false,
    height: '300px'
  };

  _headerValues = ['Zone', 'March', 'April', 'Week1', 'Week2'];

  constructor (props) {
    super(props);
    this.state = { ...this.defaultState };
    this.getTransformedList = ::this.getTransformedList;
  }

  getTransformedList () {
    const slotRevenu = this.props.list;
    const valueKey = this.props.valueKey;
    const getValue = (targetItem) => {
      const value = targetItem[valueKey];
      return value !== undefined && value !== null ? value.toFixed(2) : '';
    };

    return slotRevenu.map(item => {
      return [item.zoneName, ...item.targetDataList.map(getValue)];
    });
  }

  _renderRow (row, index) {
    console.log(row);
    return (<TableRow key={index}>
      {
        row.map((value, index) => {
          return <TableRowColumn key={index}>{value}</TableRowColumn>;
        })
      }
    </TableRow>
    );
  }

  render () {
    return (
      <div>
        <Table wrapperStyle={wrapperStyle}
          height={this.state.height}
          fixedHeader={this.state.fixedHeader}
          fixedFooter={this.state.fixedFooter}
          selectable={this.state.selectable}
          multiSelectable={this.state.multiSelectable}
        >
          <TableHeader
            displaySelectAll={this.state.showCheckboxes}
            adjustForCheckbox={this.state.showCheckboxes}
            enableSelectAll={this.state.enableSelectAll}
          >
            <TableRow>
              {
                this._headerValues.map((value, index) => {
                  return <TableHeaderColumn key={index}>{value}</TableHeaderColumn>;
                })
              }
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={this.state.showCheckboxes}
            deselectOnClickaway={this.state.deselectOnClickaway}
            showRowHover={this.state.showRowHover}
            stripedRows={this.state.stripedRows}
          >
            { this.getTransformedList().map(this._renderRow)}
          </TableBody>
        </Table>

        <div className={styles.tableActions}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <CSVLink data={this.getTransformedList()} filename={"Slot.csv"}>
              <RaisedButton primary style={{ margin: '10px' }} color label='Download' />
            </CSVLink>
          </div>
        </div>
      </div>
    );
  }
}

SlotRevenueTable.defaultProps = {
  slotRevenu: []
};

SlotRevenueTable.propTypes = {
  list: PropTypes.array.isRequired,
  valueKey: PropTypes.string.isRequired
};
export default SlotRevenueTable;
