import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { CSVLink } from 'react-csv';
import moment from 'moment';
import { getListingTableConfig } from '../modules/listing';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

const styles = {
  propContainer: {
    width: 200,
    overflow: 'hidden',
    margin: '20px auto 0',
    backgroundColor:'#000'
  },
  propToggleHeader: {
    margin: '20px auto 10px'
  }
};

const wrapperStyle = {
  marginTop:'70px',
  marginBottom:'30px'
};


class ListingTable extends PureComponent {
  defaultState = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: false,
    showCheckboxes: false,
    height: '300px'
  };

  _headerValues = ['Date', 'Zone', 'Slot', 'Status', 'Priority', 'Price', 'Surge'];

  constructor (props) {
    super(props);
    this.state = { ...this.defaultState };
    this.onClickDownload = ::this.onClickDownload;
    this.getTransformedList = ::this.getTransformedList;
  }

  handleToggle = (event, toggled) => {
    this.setState({
      [event.target.name]: toggled
    });
  };

  handleChange = (event) => {
    this.setState({ height: event.target.value });
  };

  onClickDownload () {

  }

  getTransformedList () {
    const list = this.props.list;
    return list.map(item => {
      const date = moment(item.ticket.date).format('YYYY-MM-DD ddd');
      const getValue = (value) => value != undefined ? value : '';

      return {
        date: getValue(date),
        zone: getValue(item.ticket.area.zone),
        mealTime: getValue(item.ticket.mealTime),
        status: getValue(item.status),
        position: getValue(item.position),
        price: getValue(item.price),
        surgeCode: getValue(item.surgeCod)
      };
    });
  }

  render () {
    return (
      <div>
        <Table wrapperStyle={wrapperStyle}
          height={this.state.height}
          fixedHeader={this.state.fixedHeader}
          fixedFooter={this.state.fixedFooter}
          selectable={this.state.selectable}
          multiSelectable={this.state.multiSelectable}
        >
          <TableHeader
            displaySelectAll={this.state.showCheckboxes}
            adjustForCheckbox={this.state.showCheckboxes}
            enableSelectAll={this.state.enableSelectAll}
          >
            <TableRow>
              {
                this._headerValues.map((value, index) => {
                  return <TableHeaderColumn key={index}>{value}</TableHeaderColumn>;
                })
              }
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={this.state.showCheckboxes}
            deselectOnClickaway={this.state.deselectOnClickaway}
            showRowHover={this.state.showRowHover}
            stripedRows={this.state.stripedRows}
          >
            {
              this.getTransformedList().map((row, index) => {
                return (
                  <TableRow key={index}>
                    {/*tableConig.map(config => {
                      <ListingTableCell data={row} config={config} />
                    })*/}
                    <TableRowColumn>{row.date}</TableRowColumn>
                    <TableRowColumn>{row.zone}</TableRowColumn>
                    <TableRowColumn>{row.mealTime}</TableRowColumn>
                    <TableRowColumn>{row.status}</TableRowColumn>
                    <TableRowColumn>{row.position}</TableRowColumn>
                    <TableRowColumn>{row.price}</TableRowColumn>
                    <TableRowColumn>{row.surgeCode}</TableRowColumn>
                  </TableRow>
                );
              })
            }
          </TableBody>
        </Table>

        <div className={styles.tableActions}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <CSVLink data={this.getTransformedList()} target='_blank'>
              <RaisedButton primary style={{ margin: '10px' }} color label='Download' />
            </CSVLink>
            <RaisedButton primary style={{ margin: '10px' }} label='Book' />
          </div>
        </div>
      </div>
    );
  }
}
ListingTable.propTypes = {
  list: PropTypes.array.isRequired
};
export default ListingTable;
