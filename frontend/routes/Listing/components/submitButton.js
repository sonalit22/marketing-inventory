import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import RaisedButton from 'material-ui/RaisedButton';
import styles from '../styles/Filter.scss';

class submitButton extends PureComponent {
  render () {
    const style = {
      margin: 12
    };
    const buttonStyle = classNames({
      margin: 25
    });

    return (
      <div className={styles.filterItem} >
        <RaisedButton
          buttonStyle={buttonStyle} label={this.props.label} style={style} />
      </div>
    );
  }
}

submitButton.propTypes = {
  applyFilter: PropTypes.func
};

export default submitButton;