import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import FilterContainer from '../containers/filters/FilterContainer';
import styles from '../styles/Filter.scss';
import { getFilterConfiguration } from '../modules/listing';

class FilterList extends PureComponent {
  componentDidMount () {
    this.props.changeCity('Bangalore');
  }

  render () {
    const filterConfig = getFilterConfiguration();
    return (
      <div className={styles.filterList}>
        {
        filterConfig.map(config => {
          return (
            <FilterContainer
              key={config.label}
              config={config} />
          );
        })
      }
      </div>
    );
  }
}

FilterList.propTypes = {
  changeCity: PropTypes.func.isRequired
};

export default FilterList;
