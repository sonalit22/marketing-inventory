import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import styles from '../../styles/Filter.scss';

class InputFilter extends PureComponent {
  onInputChange = (evt, value) => {
    this.props.onChange(value);
  }

  render () {
    const style = {
      width: '120px',
      height: '69px'
    };
    const floatingLabelStyle = {
      fontWeight: 100
    };

    return (
      <div className={styles.filterItem}>
        <TextField
          floatingLabelStyle={floatingLabelStyle}
          floatingLabelText={this.props.label}
          style={style}
          onChange={this.onInputChange}
                />
      </div>
    );
  }
}

InputFilter.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  filterKey: PropTypes.string,
  data: PropTypes.object,
  onChange: PropTypes.func
};

export default InputFilter;
