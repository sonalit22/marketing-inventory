import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import styles from '../../styles/Filter.scss';

class DropdownFilter extends PureComponent {
  state = {
    value: 1
  };

  handleChange = (event, index, value) => {
    this.setState({ value });
    this.props.onChange(value);
  }

  render () { 
    const style = {
      width: 130
    };

    const dropDownClass = classNames({
      [styles.filterItem]: true
    });

    const { list, valueKey, codeKey } = this.props;

    let itemList = list || [];

    itemList = itemList.map(item => {
      const code = codeKey ? item[codeKey] : item;
      const value = valueKey ? item[valueKey] : item;
      return { code, value };
    });

    const floatingLabelStyle = {
      fontWeight: 100,
      fontSize: '18px',
      left: 0,
      color: 'rgba(0,0,0,0.8)'
    };

    return (
      <div className={dropDownClass}>
        <SelectField
          floatingLabelStyle={floatingLabelStyle}
          floatingLabelText={this.props.label}
          value={this.props.value}
          onChange={this.handleChange}
          style={style}>
          {
            itemList.map(item => {
              return <MenuItem key={item.code} value={item.code} primaryText={item.value} />;
            })
          }
        </SelectField>
      </div>
    );
  }
}

DropdownFilter.defaultProps = {
  list: []
};

DropdownFilter.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
  list: PropTypes.array.isRequired,
  valueKey: PropTypes.string,
  codeKey: PropTypes.string,
  onChange: PropTypes.func
};

export default DropdownFilter;
