import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'material-ui/DatePicker';
import styles from '../../styles/Filter.scss';

class DateFilter extends PureComponent {
  constructor (props) {
    super(props);
    this.handleChange = ::this.handleChange;
  }

  handleChange = (e, date) => {
    // this.setState({value});
    this.props.onChange(date);
  }
  
  render () {
    const textFieldStyle = {
      width: '120px',
      height: '69px',
    };

    return (
            <div className={styles.filterItem}>
                <DatePicker
                    defaultDate={this.props.value}
                    hintText={this.props.label} 
                    textFieldStyle={textFieldStyle} 
                    onChange={this.handleChange}/>
            </div>
        );
    }
}

DateFilter.propTypes = {
  label: PropTypes.string,
  value: PropTypes.instanceOf(Date),
  filterKey: PropTypes.string,
  onChange: PropTypes.func,
};

export default DateFilter;  
