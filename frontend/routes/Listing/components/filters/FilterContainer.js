import React, {
  PureComponent
} from 'react';
import PropTypes from 'prop-types';
import InputFilter from './InputFilter';
import DateFilter from './DateFilter';
import DropdownFilter from './DropdownFilter';
import styles from '../../styles/Filter.scss';

import {
  FILTER_TYPES
} from '../../constants';

class FilterContainer extends PureComponent {
  constructor (props) {
    super(props);
    this._renderFilterItem = this._renderFilterItem.bind(this);
  }

  _renderFilterItem () {
    const {
      type
    } = this.props.config;

    switch (type) {
      case FILTER_TYPES.INPUT:
        return InputFilter;
      case FILTER_TYPES.DATE:
        return DateFilter;
      case FILTER_TYPES.DROPDOWN:
        return DropdownFilter;
      default:
        return null;
    }
  }

  render () {
    const Component = this._renderFilterItem();
    const {
      label,
      filterKey,
      stateKey,
      onChange
    } = this.props.config;
    const onChangeFunc = this.props[onChange];
    const _state = this.props.filter[stateKey];

    return ( <div className = { styles.filterContainer} >
      <Component label = {label}
        filterKey = {filterKey} 
        { ..._state}
        onChange = {onChangeFunc}/>
      </div>
    );
  }
}

FilterContainer.propTypes = {
  config: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired
};

export default FilterContainer;