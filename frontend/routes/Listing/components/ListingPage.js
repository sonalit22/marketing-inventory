import React from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import styles from '../styles/Listing.scss';
import Header from 'components/CommonHeader';
import FilterList from '../containers/FilterList';
import ListingTable from '../containers/ListingTable';
import SlotRevenueTable from '../containers/SlotRevenuTable';

class ListingPage extends React.Component {
  constructor (props) {
    super(props);
    this.onSubmit = ::this.onSubmit;
  }

  onSubmit () {
    this.props.fetchListing();
  }

  render () {
    return (
      <div>
        <Header />
        <div>
          <FilterList />
          <RaisedButton primary label='Submit' onClick={this.onSubmit} />
        </div>
        <div style={{ width: '60%' }}>
          <ListingTable />
        </div>
        <div style={{ width: '38%', float: 'right', marginTop: '-513px' }}>
          <h5 style={{ textAlign: 'left' }}>Target Revenue</h5>
          <SlotRevenueTable valueKey='adRevenue' />
          <h5>Slot</h5>
          <SlotRevenueTable valueKey='fillRateRevenue' />
        </div>
      </div>
    );
  }
}

ListingPage.propTypes = {
  fetchListing: PropTypes.func.isRequired
};

export default ListingPage;
