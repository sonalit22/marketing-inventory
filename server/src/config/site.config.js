var defaultsDeep = require('lodash/defaultsDeep');

var config = {
  base: {
    api: 'bac-u-0015-api-server-01.swiggyops.de:8053',
    https_enabled: false
  },
  development: {
  },
  uat: {
  },
  production: {
  }
};

var env = process.env.NODE_ENV || "development";
config[env] = defaultsDeep({}, config[env], config.base);
module.exports = config[env];
