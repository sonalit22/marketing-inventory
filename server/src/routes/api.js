var siteConfig = require('../config/site.config');
var rp = require('request-promise');
var extend = require('lodash/extend');
var get = require('lodash/get');
var set = require('lodash/set');

function getProxyTarget(config) {
    var protocol = config.https_enabled ? 'https': 'http';
    var target = protocol + '://' +  config.api;
    return target;
  }

module.exports = {
    proxyHost: function () {
      return function (req, res, next) {
        req.api_host = getProxyTarget(siteConfig);
        next();
      };
    },

  proxy: function (req, res) {
    var host = req.api_host;

    var options = {
      method: req.method,
      uri: host + req.url,
      headers: {
       "Content-Type": "application/json;charset=utf-8"
      },
      transform: function(body, res) {
        res.data = JSON.parse(body);
        return res;
      }
    };

      // replace response transform if exists
      if (req.transform && typeof(req.transform) === "function") {
        options.transform = req.transform
      }

      var customHeaders = ['sessionid', 'userid'];
      customHeaders.forEach(function (header) {
        if (req.headers[header]) {
          options.headers[header] = req.headers[header];
        }
      });

      if (req.method === "POST" || req.method === "PUT" || req.method === "DELETE") {
          options = extend(options, {
              body: JSON.stringify(req.body)
          });
      }

    console.log(options);
    rp(options)
      .then(response => {
        customHeaders.forEach(function (header) {
          if (response.headers[header]) {
            res.header(header, response.headers[header]);
          }
        });

        res.send(response.data);
      }).catch(err => {
        if (!(err && err.statusCode)) {
          err = {
            statusCode: 504,
            error: 'Something went wrong'
          };
        }

        res.status(err.statusCode).send({
          status: 0,
          message: err.error
        });
      });
  }
};
