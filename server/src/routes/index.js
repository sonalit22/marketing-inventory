var express = require('express');
var router = express.Router();

var api = require('./api');
var proxyHost = api.proxyHost;
var proxy = api.proxy;

router.all('/*', proxyHost(), proxy);

module.exports = router;
